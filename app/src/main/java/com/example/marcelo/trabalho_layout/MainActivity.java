package com.example.marcelo.trabalho_layout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btn_frame;
    private Button btn_grid;
    private Button btn_table;
    private Button btn_relative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeFields();

        btn_frame.setOnClickListener(new AbrirFrame());
        btn_grid.setOnClickListener(new AbrirGrid());
        btn_table.setOnClickListener(new AbrirTable());
        btn_relative.setOnClickListener(new AbrirRelative());
    }

    private void initializeFields() {
        btn_frame = (Button) findViewById(R.id.btn_frame);
        btn_grid = (Button) findViewById(R.id.btn_grid);
        btn_table = (Button) findViewById(R.id.btn_table);
        btn_relative = (Button) findViewById(R.id.btn_relative);
    }


    private class AbrirGrid implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, GridLayout.class);
            startActivity(intent);
        }

    }

    private class AbrirTable implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, TableLayout.class);
            startActivity(intent);
        }

    }

    private class AbrirFrame implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, FrameLayout.class);
            startActivity(intent);
        }

    }

    private class AbrirRelative implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, RelativeLayout.class);
            startActivity(intent);
        }

    }
}
